package com.ristek.emergere.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Mamen on 11/3/2016.
 */

public class EmergereRequestQueue{
    private static RequestQueue requestQueue;

    public static RequestQueue getInstance(Context context){
        if(requestQueue == null){
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

}
