package com.ristek.emergere.features;

/**
 * Created by Mamen on 11/1/2016.
 */

public class Utils {

    public static final String URL = "URL";

    public static final String OK_STATUS = "OK";
    public static final String ORIGIN = "ORIGIN";
    public static final String DESTINATION = "DESTINATION";

    public static final long MIN_TIME = 3000;
    public static final float MIN_DISTANCE = 1;
    public static final String HOSPITAL = "hospital";
    public static final String POLICE = "police";
    public static final String FIREFIGTER = "fire_station";
    public static final String THREE_NEARST_INSTANCE = "three nearst instance";
    public static final String PLACE_ID = "place id";
    public static final String PLACE_TYPE = "place type";
    public static final String PLACE_NEARST = "place nearst";
    public static final String NEARST_INSTANCES_PHONE = "nearst instances phone";

}
