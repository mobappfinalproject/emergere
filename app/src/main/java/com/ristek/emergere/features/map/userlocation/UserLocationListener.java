package com.ristek.emergere.features.map.userlocation;

import android.location.Location;

/**
 * Created by Mamen on 12/7/2016.
 */

public interface UserLocationListener {
    void locationChange(Location location);
}
