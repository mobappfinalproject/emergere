package com.ristek.emergere.features.detailinstance;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.ristek.emergere.R;
import com.ristek.emergere.features.Utils;
import com.ristek.emergere.features.detailinstance.event.DetailInstanceEvent;
import com.ristek.emergere.model.Instance;
import com.ristek.emergere.network.EmergereRequestQueue;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;


public class DetailInstanceService extends IntentService {

    public DetailInstanceService() {
        super("DetailInstanceService");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        if (intent != null) {
            final String placeID = intent.getStringExtra(Utils.PLACE_ID);
            StringBuilder urlBuilder = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?placeid=")
                .append(placeID).append("&key=").append(getString(R.string.google_place_key));
            StringRequest stringRequest = new StringRequest(Request.Method.GET, urlBuilder.toString()
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("DetailInstanceService","RESPONSE: "+response);
                    try {
                        JSONObject root = new JSONObject(response);
                        if(root.getString("status").equals("OK")){
                            JSONObject result = root.getJSONObject("result");
                            String name = result.getString("name");
                            String type = intent.getStringExtra(Utils.PLACE_TYPE);
                            String address = result.getString("adr_address");
                            String phone = result.getString("international_phone_number").replace(" ","");
                            String website = null;
                            String hourOfOperation = null;
                            JSONObject loc = result.getJSONObject("geometry").getJSONObject("location");
                            LatLng location = new LatLng(loc.getDouble("lat"),loc.getDouble("lng"));
                            Instance instance = new Instance(name, type, address, phone, website, hourOfOperation, location, placeID);
                            EventBus.getDefault().post(new DetailInstanceEvent(instance));
                            Log.d("TELEPONNN","DETAIL, TYPE "+type+" .");
                        }else{
                            Log.d("DetailInstanceService","STATUS: GAGAL"+root.getString("status"));
                        }
                    } catch (JSONException e) {
                        if(e.getMessage().equals("No value for international_phone_number")){
                            Toast.makeText(DetailInstanceService.this,"data nomor telephone tidak ada",Toast.LENGTH_LONG).show();
                        }
                        Log.d("DetailInstanceService","ERROR: "+e.getMessage());
                    }
                }
            }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            EmergereRequestQueue.getInstance(this).add(stringRequest);

            Log.d("DetailInstanceService","URL: "+urlBuilder.toString());
        }
    }

}
