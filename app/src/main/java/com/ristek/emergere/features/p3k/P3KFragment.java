package com.ristek.emergere.features.p3k;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ristek.emergere.R;
import com.ristek.emergere.model.P3K;
import com.ristek.emergere.network.EmergereRequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lightmire on 10/31/2016.
 */

public class P3KFragment extends Fragment {
    private final String TAG = P3KFragment.class.getSimpleName();
    @BindView(R.id.p3k_list)
    RecyclerView p3kList;

    private RequestQueue mRequest;
    private String p3kListUrl;
    private P3KAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        p3kListUrl = getString(R.string.p3k_list_url);
        mRequest = EmergereRequestQueue.getInstance(getContext());
        adapter = new P3KAdapter(getContext(), new ArrayList<P3K>());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p3k, container, false);
        ButterKnife.bind(this, view);
        mRequest.add(new JsonObjectRequest(Request.Method.GET, p3kListUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    String videoUrl = response.getString("videoUrl");
                    String category = response.getString("category");
                    String title = response.getString("title");
                    JSONArray stepArray = response.getJSONArray("steps");
                    ArrayList<String> steps = new ArrayList<String>();
                    if (stepArray != null) {
                        for (int i = 0; i < stepArray.length(); i++) {
                            steps.add(stepArray.getString(i));
                        }
                    }
                    P3K p3k = new P3K(title, category, videoUrl, steps);
                    ArrayList<P3K> p3kData = new ArrayList<P3K>();
                    p3kData.add(p3k);
                    adapter.setData(p3kData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
            }
        }));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ArrayList<P3K> data = Utils.dummyP3K();
        p3kList.setLayoutManager(new LinearLayoutManager(getContext()));
        p3kList.setAdapter(adapter);
    }
}
