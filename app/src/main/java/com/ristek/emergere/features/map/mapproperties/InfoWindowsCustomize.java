package com.ristek.emergere.features.map.mapproperties;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ristek.emergere.R;
import com.ristek.emergere.features.Utils;
import com.ristek.emergere.model.Instance;

import java.util.Map;

/**
 * Created by Mamen on 10/28/2016.
 */

public class InfoWindowsCustomize implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private String userMarkerId;
    private Map<String, Instance> instances;

    public InfoWindowsCustomize(Context context) {
        this.context = context;
    }

    public void setUserMarkerId(String userMarkerId) {
        this.userMarkerId = userMarkerId;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        if(marker.getId().equals(userMarkerId)){
            return null;
        }else{
            //mendapatkan placeId dan mengambil instance berdacarkan place idnya
            String placeId = marker.getTitle();

            if (instances != null){
                Instance instance = instances.get(placeId);
                if(instance != null) {
                    View view = LayoutInflater.from(context).inflate(R.layout.info_window, null, false);
                    TextView instanceNameTV = (TextView) view.findViewById(R.id.instanceNameTV);
                    TextView instanceCategoryTV = (TextView) view.findViewById(R.id.instanceTypeTV);
                    ImageView instanceIconIV = (ImageView) view.findViewById(R.id.instanceIconIV);

                    instanceNameTV.setText(instance.getName());
                    instanceIconIV.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.mipmap.ic_phone,null));

                    int category = -1;
                    switch (instance.getType()){
                        case Utils.HOSPITAL:
                            category = R.string.hospital;
                            break;
                        case Utils.POLICE:
                            category = R.string.police;
                            break;
                        case Utils.FIREFIGTER:
                            category = R.string.firefighters;
                            break;
                    }
                    if (category != -1) instanceCategoryTV.setText(context.getString(category));

                    return view;
                }
            }
        }

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        if(marker == null){
            return null;
        }
        return null;
    }

    public void setInstances(Map<String, Instance> instances){
        this.instances = instances;
    }

}
