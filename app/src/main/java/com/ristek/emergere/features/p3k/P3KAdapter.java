package com.ristek.emergere.features.p3k;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ristek.emergere.R;
import com.ristek.emergere.model.P3K;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lightmire on 10/26/2016.
 */

public class P3KAdapter extends RecyclerView.Adapter<P3KAdapter.P3KHolder> {
    private ArrayList<P3K> listOfP3K;
    private Context mContext;
    private static final int DETAIL_TYPE = 1;
    private static final int LIST_TYPE = 2;
    private int mType;

    public P3KAdapter(Context context, ArrayList<P3K> data) {
        this.mContext = context;
        this.listOfP3K = data;
    }

     static class P3KHolder extends RecyclerView.ViewHolder {
         View root;
         @BindView(R.id.p3k_title) TextView titleView;
         @BindView(R.id.p3k_category) TextView categoryView;

         public P3KHolder (View v) {
             super(v);
             root = v;
             titleView = (TextView) v.findViewById(R.id.p3k_title);
             categoryView = (TextView) v.findViewById(R.id.p3k_category);
             ButterKnife.bind(this, v);
         }
    }

    @Override
    public int getItemViewType(int position) {
        return mType;
    }

    @Override
    public P3KHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.p3k_item_list, parent, false);
        return new P3KHolder(view);
    }

    @Override
    public void onBindViewHolder(P3KHolder holder, int position) {
        P3K p3k = listOfP3K.get(position);
        Log.d("TAG", p3k.getTitle());
        Log.d("TAG", holder.toString());
            holder.titleView.setText(p3k.getTitle());
            holder.categoryView.setText(p3k.getCategory());
            holder.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // NOTE : intent to p3k detail activity
                    Intent intent = new Intent(mContext, P3KDetail.class);
                    mContext.startActivity(intent);
                }
            });
        }

    public void setData(ArrayList<P3K> p3kData) {
        this.listOfP3K = p3kData;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return listOfP3K.size();
    }
}
