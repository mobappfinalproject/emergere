package com.ristek.emergere.features.map.nearbyistances;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.ristek.emergere.features.Utils;
import com.ristek.emergere.features.map.event.NearbyInstancesEvent;
import com.ristek.emergere.model.Instance;
import com.ristek.emergere.network.EmergereRequestQueue;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NearbyInstancesService extends IntentService {

    public NearbyInstancesService() {
        super("NearbyInstancesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String url = intent.getStringExtra(Utils.URL);

            Log.d("URL_NEARBY",url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("NearbyInstancesService","RESPONSE: "+url);
                    try {
                        JSONObject root = new JSONObject(response);
                        if(root.getString("status").equals("OK")){
                            Map<String, Instance> instances = new HashMap<>();
                            JSONArray results = root.getJSONArray("results");
                            for(int i = 0 ; i < results.length() ; i++){
                                JSONObject result = results.getJSONObject(i);
                                JSONArray types = result.getJSONArray("types");

                                if(types.length() == 3
                                        && (types.getString(0).equals("hospital")
                                            || types.getString(0).equals("police")
                                            ||types.getString(0).equals("fire_station"))
                                        && types.getString(1).equals("point_of_interest")
                                        && types.getString(2).equals("establishment")){

                                    Instance instance = new Instance(result.getString("name")
                                            ,types.getString(0)
                                            ,new LatLng(result
                                                .getJSONObject("geometry")
                                                .getJSONObject("location")
                                                .getDouble("lat")
                                            ,result.getJSONObject("geometry")
                                                .getJSONObject("location")
                                                .getDouble("lng"))
                                            ,result.getString("place_id"));

                                    instances.put(instance.getPlaceId(),instance);
                                }

                            }
                            EventBus.getDefault().post(new NearbyInstancesEvent(instances));
                        }
                    } catch (JSONException e) {

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NearbyInstancesService","ERROR: "+error.getMessage());
                }
            });

            EmergereRequestQueue.getInstance(this).add(stringRequest);

        }
    }
}
