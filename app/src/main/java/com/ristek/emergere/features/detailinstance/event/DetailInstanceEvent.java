package com.ristek.emergere.features.detailinstance.event;

import com.ristek.emergere.model.Instance;

/**
 * Created by Mamen on 12/7/2016.
 */

public class DetailInstanceEvent {
    private Instance instance;

    public DetailInstanceEvent(Instance instance) {
        this.instance = instance;
    }

    public Instance getInstance() {
        return instance;
    }
}
