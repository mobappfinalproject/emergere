package com.ristek.emergere.features.map.addressconverter;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.ristek.emergere.features.map.MapFragment;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddressConverterService extends IntentService {

    public AddressConverterService() {
        super("AddressConverterService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            Location userCurrentLocation = intent.getParcelableExtra(MapFragment.USER_CURRENT_LOCATION);
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());

            try {
                List<Address> addresses = geocoder.getFromLocation(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude(), 1);
                if(!addresses.isEmpty()){

                    Address cAddress = addresses.get(0);
                    StringBuilder address = new StringBuilder();
                    for(int i = 0 ; i < cAddress.getMaxAddressLineIndex() ; i++){
                        address.append(cAddress.getAddressLine(i)).append(" ");
                    }
                    EventBus.getDefault().post(new AddressConverterEvent(true, address.toString().trim()));

                    return;
                }else{
                    EventBus.getDefault().post(new AddressConverterEvent(false, null));
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("GEOLOCATIONNNN","oh no it is error man :( "+e.getMessage());
            }

        }

    }

}
