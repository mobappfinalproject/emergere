package com.ristek.emergere.features.p3k;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.ristek.emergere.R;
import com.ristek.emergere.model.P3K;
import com.ristek.emergere.network.EmergereRequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class P3KDetail extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private final String TAG = P3KDetail.class.getSimpleName();
    @BindView(R.id.video_player)
    YouTubePlayerView videoPlayer;
    @BindView(R.id.p3k_title)
    TextView p3kTitleView;
    @BindView(R.id.p3k_category)
    TextView p3kCategoryView;
    @BindView(R.id.step_list)
    RecyclerView stepListView;

    private String p3kUrl;
    private RequestQueue mRequest;
    private String videoUrl;
    private P3KStepAdapter stepAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p3k_detail);
        ButterKnife.bind(this);
        String API_KEY = getString(R.string.google_maps_key);
        videoPlayer.initialize(API_KEY, this);
        // NOTE : load video here
        p3kUrl = getString(R.string.p3k_url);
        mRequest = EmergereRequestQueue.getInstance(this);
        mRequest.add(new JsonObjectRequest(Request.Method.GET, p3kUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    videoUrl = response.getString("videoUrl");
                    String category = response.getString("category");
                    String title = response.getString("title");
                    JSONArray stepArray = response.getJSONArray("steps");
                    p3kTitleView.setText(title);
                    p3kCategoryView.setText(category);
                    ArrayList<String> steps = new ArrayList<String>();
                    if (stepArray != null) {
                        for (int i = 0; i < stepArray.length(); i++) {
                            steps.add(stepArray.getString(i));
                        }
                    }
                    P3KStepAdapter adapter = new P3KStepAdapter(steps);
                    stepListView.setAdapter(adapter);
                    stepListView.setLayoutManager(new LinearLayoutManager(P3KDetail.this));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
            }
        }));
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.loadVideo(videoUrl);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {
        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {
        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    };

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onPlaying() {

        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onStopped() {
        }

        @Override
        public void onBuffering(boolean b) {
        }

        @Override
        public void onSeekTo(int i) {

        }
    };
}
