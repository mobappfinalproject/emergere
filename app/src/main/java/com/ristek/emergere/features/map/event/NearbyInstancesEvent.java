package com.ristek.emergere.features.map.event;

import com.ristek.emergere.model.Instance;

import java.util.List;
import java.util.Map;

/**
 * Created by Mamen on 11/3/2016.
 */

public class NearbyInstancesEvent {

    private Map<String, Instance> instances;

    public NearbyInstancesEvent(Map<String, Instance> instances) {
        this.instances = instances;
    }

    public Map<String, Instance> getInstances() {
        return instances;
    }
}
