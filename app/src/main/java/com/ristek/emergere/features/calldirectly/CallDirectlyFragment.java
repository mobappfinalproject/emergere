package com.ristek.emergere.features.calldirectly;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ristek.emergere.R;
import com.ristek.emergere.features.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallDirectlyFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.callHospitalBtn) LinearLayout callHospitalBtn;
    @BindView(R.id.callPoliceBtn) LinearLayout callPoliceBtn;
    @BindView(R.id.callFirefighterBtn) LinearLayout callFirefighterBtn;
    Unbinder unbinder;


    public CallDirectlyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_call_directly, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    private void setListener() {
        callHospitalBtn.setOnClickListener(this);
        callPoliceBtn.setOnClickListener(this);
        callFirefighterBtn.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        String key = null;
        switch (v.getId()){
            case R.id.callHospitalBtn:
                key = Utils.HOSPITAL;
                break;
            case R.id.callPoliceBtn:
                key = Utils.POLICE;
                break;
            case R.id.callFirefighterBtn:
                key = Utils.FIREFIGTER;
                break;
        }
        if(key != null ){
            SharedPreferences preferences = getContext().getApplicationContext().getSharedPreferences(Utils.NEARST_INSTANCES_PHONE, Context.MODE_PRIVATE);
            String instancePhone = preferences.getString(key,null);
            if(instancePhone != null){call(instancePhone);}
            else{Toast.makeText(getContext(),"data nomor telephone tidak ada",Toast.LENGTH_LONG).show();}
        }else{
            Toast.makeText(getContext(),"KEY TIDAK ADA",Toast.LENGTH_LONG).show();
        }
    }

    private void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

}
