package com.ristek.emergere.features.map.mapproperties;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Mamen on 10/28/2016.
 */

public class CameraChangeListener implements GoogleMap.OnCameraChangeListener {

    private Marker marker;

    public void setMarker(Marker marker){
        this.marker = marker;
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if(marker != null && !marker.isInfoWindowShown()){
            marker.showInfoWindow();
        }
    }
}
