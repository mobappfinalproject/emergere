package com.ristek.emergere.features.p3k;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ristek.emergere.R;

import java.util.ArrayList;

/**
 * Created by lightmire on 12/9/2016.
 */

public class P3KStepAdapter extends RecyclerView.Adapter<P3KStepAdapter.ViewHolder> {
    ArrayList<String> steps;

    public P3KStepAdapter(ArrayList<String> steps) {
        this.steps = steps;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        View root;
        TextView numView;
        TextView descriptionView;
        public ViewHolder (View v) {
            super(v);
            root = v;
            numView = (TextView) v.findViewById(R.id.step_num);
            descriptionView = (TextView) v.findViewById(R.id.step_description);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.p3k_item_step, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String step = steps.get(position);
        holder.numView.setText("" + (position + 1));
        holder.descriptionView.setText(step);
    }

    @Override
    public int getItemCount() {
        return steps.size();
    }


}
