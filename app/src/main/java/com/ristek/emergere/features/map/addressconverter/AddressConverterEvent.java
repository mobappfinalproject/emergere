package com.ristek.emergere.features.map.addressconverter;

/**
 * Created by Mamen on 10/29/2016.
 */

public class AddressConverterEvent {

    private boolean isSuccess;
    private String address;

    public AddressConverterEvent(boolean isSuccess, String address) {
        this.isSuccess = isSuccess;
        this.address = address;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getAddress() {
        return address;
    }
}