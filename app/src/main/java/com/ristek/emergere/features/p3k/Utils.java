package com.ristek.emergere.features.p3k;

import com.ristek.emergere.model.P3K;

import java.util.ArrayList;

/**
 * Created by Lightmire on 10/31/2016.
 */

public class Utils {
    public static ArrayList<P3K> dummyP3K() {
        ArrayList<P3K> data = new ArrayList<P3K>();
        data.add(new P3K("Luka bakar", "Derajat 1, 2, 3", "MTG1zl8PkXk", new ArrayList<String>()));
        data.add(new P3K("Luka bakar", "Derajat 1, 2, 3", "MTG1zl8PkXk", new ArrayList<String>()));
        data.add(new P3K("Luka bakar", "Derajat 1, 2, 3", "MTG1zl8PkXk", new ArrayList<String>()));
        data.add(new P3K("Luka bakar", "Derajat 1, 2, 3", "MTG1zl8PkXk", new ArrayList<String>()));
        data.add(new P3K("Luka bakar", "Derajat 1, 2, 3", "MTG1zl8PkXk", new ArrayList<String>()));
        return data;
    }
}
