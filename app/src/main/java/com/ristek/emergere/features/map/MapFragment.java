package com.ristek.emergere.features.map;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ristek.emergere.R;
import com.ristek.emergere.features.Utils;
import com.ristek.emergere.features.detailinstance.DetailInstanceService;
import com.ristek.emergere.features.detailinstance.event.DetailInstanceEvent;
import com.ristek.emergere.features.map.addressconverter.AddressConverterEvent;
import com.ristek.emergere.features.map.addressconverter.AddressConverterService;
import com.ristek.emergere.features.map.event.NearbyInstancesEvent;
import com.ristek.emergere.features.map.mapproperties.CameraChangeListener;
import com.ristek.emergere.features.map.mapproperties.InfoWindowsCustomize;
import com.ristek.emergere.features.map.nearbyistances.NearbyInstancesService;
import com.ristek.emergere.features.map.userlocation.UserLocation;
import com.ristek.emergere.features.map.userlocation.UserLocationListener;
import com.ristek.emergere.model.Instance;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.ristek.emergere.features.Utils.NEARST_INSTANCES_PHONE;


/**
 * Created by Muhammad Umar Farisi
 */
public class MapFragment extends Fragment implements OnMapReadyCallback
        , CompoundButton.OnCheckedChangeListener, UserLocationListener {

    public static final String USER_CURRENT_LOCATION = "USER CURRENT LOCATION";

    @BindView(R.id.hospitalCB) CheckBox hospitalCB;
    @BindView(R.id.policeCB) CheckBox policeCB;
    @BindView(R.id.firefightersCB) CheckBox firefightersCB;
    private Unbinder unbinder;

    private GoogleMap mMap;
    private InfoWindowsCustomize infoWindowsCustomize;
    private Marker userCurrentMarker;
    private Location mLastLocation;

    private List<Marker> hospitalMarker;
    private List<Marker> policeMarker;
    private List<Marker> firefighterMarker;
    private Map<String, Instance> nearstInstance;
    private UserLocation userLocation;

    @Override
    public View onCreateView(LayoutInflater inflater
            , ViewGroup container
            , Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //menyetel struktur data untuk 3 instansi terdekat dengan tipe yg berbeda2
        nearstInstance = new HashMap<>();

        //menyetel listener untuk semua checkbox
        hospitalCB.setOnCheckedChangeListener(this);
        policeCB.setOnCheckedChangeListener(this);
        firefightersCB.setOnCheckedChangeListener(this);

        //menyetel map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        userLocation = new UserLocation(getContext());
        userLocation.setListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //menyetel objek googlemap
        infoWindowsCustomize = new InfoWindowsCustomize(getContext());
        infoWindowsCustomize.setUserMarkerId(getString(R.string.your_location));
        mMap = googleMap;
        mMap.setInfoWindowAdapter(infoWindowsCustomize);

        //menyetel camera
        mMap.setOnCameraChangeListener(new CameraChangeListener());

        //menyembunyikan tombol-tombol default dari google map
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);

        //menyetel event listener untuk infowindow
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String placeId = marker.getTitle();
                getDetailInstance(placeId,null,false);
            }
        });

    }

    private void getDetailInstance(String placeId, String placeType, boolean isNearstInstance) {
        Intent intent = new Intent(MapFragment.this.getActivity(), DetailInstanceService.class);
        intent.putExtra(Utils.PLACE_ID, placeId);
        intent.putExtra(Utils.PLACE_TYPE, placeType);
        intent.putExtra(Utils.PLACE_NEARST, isNearstInstance);
        MapFragment.this.getActivity().startService(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDetailInstance(DetailInstanceEvent event){
        final Instance instance = event.getInstance();
        boolean isNearst = instance.getType() != null;
        if(isNearst){
            nearstInstance.put(instance.getType(),instance);
            SharedPreferences preferences = getContext()
                    .getApplicationContext()
                    .getSharedPreferences(NEARST_INSTANCES_PHONE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(instance.getType(),instance.getPhone());
            editor.apply();
        }else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.contact) + instance.getName() + "?");
            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    call(instance.getPhone());
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
    }

    private void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.equals(hospitalCB)){
            if(isChecked) {
                showMarkers(hospitalMarker);
            }else{
                hideMarkers(hospitalMarker);
            }
        }else if(buttonView.equals(policeCB)){
            if(isChecked) {
                showMarkers(policeMarker);
            }else{
                hideMarkers(policeMarker);
            }
        }else if(buttonView.equals(firefightersCB)){
            if(isChecked) {
                showMarkers(firefighterMarker);
            }else{
                hideMarkers(firefighterMarker);
            }
        }
    }

    private void hideMarkers(List<Marker> markers) {
        if(markers != null){
            for(Marker marker : markers){
                marker.setVisible(false);
            }
        }
    }

    private void showMarkers(List<Marker> markers) {
        if(markers != null){
            for(Marker marker : markers){
                marker.setVisible(true);
            }
        }
    }

    @Override
    public void locationChange(Location location){
        mLastLocation = location;
        Location oldLocation = null;
        if (userCurrentMarker != null) {
            oldLocation = new Location("");
            oldLocation.setLatitude(userCurrentMarker.getPosition().latitude);
            oldLocation.setLongitude(userCurrentMarker.getPosition().longitude);
            userCurrentMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        userCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(getString(R.string.your_location))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //mencari alamat user
        Intent intentAddressUserLocation = new Intent(getContext(), AddressConverterService.class);
        intentAddressUserLocation.putExtra(USER_CURRENT_LOCATION, location);
        getActivity().startService(intentAddressUserLocation);

        if(oldLocation == null || (oldLocation != null && oldLocation.distanceTo(mLastLocation) > 100)){
            //menyetel lokasi terdekat
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key="+getString(R.string.google_place_key)
                    +"&location="+userCurrentMarker.getPosition().latitude+","+userCurrentMarker.getPosition().longitude
                    +"&radius=5000&types="+ Utils.HOSPITAL+"|"+Utils.POLICE+"|"+Utils.FIREFIGTER+"&language=in";
            Intent intentNearbyLocation = new Intent(getContext(), NearbyInstancesService.class);
            intentNearbyLocation.putExtra(Utils.URL, url);
            getActivity().startService(intentNearbyLocation);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserAddressResult(AddressConverterEvent addressConverterEvent){
        if(addressConverterEvent.isSuccess()){
            infoWindowsCustomize.setUserMarkerId(addressConverterEvent.getAddress());
            userCurrentMarker.setTitle(addressConverterEvent.getAddress());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNearbyInstances(NearbyInstancesEvent event){

        nearstInstance = new HashMap<>();

        Map<String, Instance> instances = event.getInstances();
        Map<String, Instance> instancesForInfoWindow = new HashMap<>();
        hospitalMarker = new ArrayList<>();
        policeMarker = new ArrayList<>();
        firefighterMarker = new ArrayList<>();

        //hapus semua marker dan masukan marker user
        mMap.clear();
        userCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(userCurrentMarker.getPosition().latitude
                        , userCurrentMarker.getPosition().longitude)));

        //objek untuk camera map
        LatLngBounds.Builder focus = new LatLngBounds.Builder();

        //membuat semua marker pada map
        Set<Map.Entry<String, Instance>> entries = instances.entrySet();
        for(Map.Entry<String, Instance> entry : entries){

            Instance instance = entry.getValue();
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .title(instance.getPlaceId())
                    .position(instance.getLocation()));
            int iconInstance = -1;
            switch (instance.getType()){
                case Utils.HOSPITAL:
                    iconInstance = R.mipmap.ic_hospital;
                    hospitalMarker.add(marker);
                    break;
                case Utils.POLICE:
                    iconInstance = R.mipmap.ic_police;
                    policeMarker.add(marker);
                    break;
                case Utils.FIREFIGTER:
                    iconInstance = R.mipmap.ic_firefighter;
                    firefighterMarker.add(marker);
                    break;
            }
            if(iconInstance != -1)marker.setIcon(BitmapDescriptorFactory.fromResource(iconInstance));

            focus.include(marker.getPosition());

            //add instance untuk digunakan di infoWindows
            instancesForInfoWindow.put(instance.getPlaceId(), instance);

            //add instance untuk instance yg terdekat
            if(!nearstInstance.containsKey(instance.getType())){
                nearstInstance.put(instance.getType(), instance);
            }else{
                //menyetel lokasi instance yg sudah disimpan
                Instance old = nearstInstance.get(instance.getType());
                Location oldLocation = new Location("");
                oldLocation.setLatitude(old.getLocation().latitude);
                oldLocation.setLongitude(old.getLocation().longitude);
                //menyetel lokasi instance yg ingin dicoba
                Location newLocation = new Location("");
                newLocation.setLatitude(instance.getLocation().latitude);
                newLocation.setLongitude(instance.getLocation().longitude);
                //menyetel lokasi instance yg sudah disimpan
                mLastLocation.setLatitude(userCurrentMarker.getPosition().latitude);
                mLastLocation.setLongitude(userCurrentMarker.getPosition().longitude);

                if(mLastLocation.distanceTo(oldLocation) > mLastLocation.distanceTo(newLocation)){
                    nearstInstance.put(instance.getType(), instance);
                }
            }

        }

        collectShortestInstance();

        //set camera pada map sehingga semua marker terlihat
        if(!instances.isEmpty()){
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(focus.build(),0));

            //set data for infoWindows
            infoWindowsCustomize.setInstances(instancesForInfoWindow);

            //set checked for all cb
            hospitalCB.setChecked(true);
            policeCB.setChecked(true);
            firefightersCB.setChecked(true);
        }

    }

    private void collectShortestInstance(){
        Set<Map.Entry<String, Instance>> entries = nearstInstance.entrySet();
        for(Map.Entry<String, Instance> entry: entries){
            String placeId = entry.getValue().getPlaceId();
            String placeType = entry.getKey();
            getDetailInstance(placeId,placeType,true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        infoWindowsCustomize = null;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }


}

