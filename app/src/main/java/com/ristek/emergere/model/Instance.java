package com.ristek.emergere.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Instance implements Serializable{

    private String name;
    private String address;
    private String phone;
    private String website;
    private String hourOfOperation;
    private LatLng location;
    private String placeId;
    private String type;

    public Instance(String name, String type, String address, String phone, String website, String hourOfOperation, LatLng location, String placeId) {
        this.name = name;
        this.type = type;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.hourOfOperation = hourOfOperation;
        this.location = location;
        this.placeId = placeId;
    }

    public Instance(String name, String type, LatLng location, String placeId){
        this(name,type,null,null,null,null,location,placeId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getHourOfOperation() {
        return hourOfOperation;
    }

    public void setHourOfOperation(String hourOfOperation) {
        this.hourOfOperation = hourOfOperation;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Instance{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", website='" + website + '\'' +
                ", hourOfOperation='" + hourOfOperation + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instance instance = (Instance) o;

        if (!name.equals(instance.name)) return false;
        if (!address.equals(instance.address)) return false;
        if (!phone.equals(instance.phone)) return false;
        if (website != null ? !website.equals(instance.website) : instance.website != null)
            return false;
        if (hourOfOperation != null ? !hourOfOperation.equals(instance.hourOfOperation) : instance.hourOfOperation != null)
            return false;
        return location.equals(instance.location);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (hourOfOperation != null ? hourOfOperation.hashCode() : 0);
        result = 31 * result + location.hashCode();
        return result;
    }
}
