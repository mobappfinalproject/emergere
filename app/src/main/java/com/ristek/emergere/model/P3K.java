package com.ristek.emergere.model;

import java.util.List;


public class P3K {

    private String title;
    private String category;
    private String videoUrl;
    private List<String> steps;

    public P3K(String title, String category, String videoUrl, List<String> steps) {
        this.title = title;
        this.category = category;
        this.videoUrl = videoUrl;
        this.steps = steps;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "P3K{" +
                "title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", steps=" + steps +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        P3K p3K = (P3K) o;

        if (!title.equals(p3K.title)) return false;
        return category.equals(p3K.category);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + category.hashCode();
        return result;
    }
}
