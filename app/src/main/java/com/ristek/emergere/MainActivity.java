package com.ristek.emergere;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.ristek.emergere.features.calldirectly.CallDirectlyFragment;
import com.ristek.emergere.features.p3k.P3KFragment;
import com.ristek.emergere.features.map.MapFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        setUpViewPager();

        tabLayout.setupWithViewPager(viewPager);

    }

    private void setUpViewPager()  {

        final List<Fragment> fragments = new ArrayList<>();
        fragments.add(new CallDirectlyFragment());
        fragments.add(new MapFragment());
        fragments.add(new P3KFragment());

        final List<String> titles = new ArrayList<>();
        titles.add(getString(R.string.nearest));
        titles.add(getString(R.string.map));
        titles.add(getString(R.string.p3k));

        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titles.get(position);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
